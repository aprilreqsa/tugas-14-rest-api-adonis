/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})
// Route.post('/venues', 'VenuesController.store').as('venues.store')
// Route.post('/bookings', 'VenuesController.booking').as("bookings.booking")
// Route.get('/venues', 'VenuesController.index').as('venues.index')
// Route.get('/venues/:id', 'VenuesController.show').as('venues.show')
// Route.put('/venues/:id', 'VenuesController.update').as('venues.update')
// Route.delete('/venues/:id', 'VenuesController.destroy').as('venues.destroy')
// Route.post('/fields', 'FieldsController.store').as('fields.store')
// Route.get('/fields', 'FieldsController.index').as('fields.index')
// Route.get('/fields/:id', 'FieldsController.show').as('fields.show')
// Route.put('/fields/:id', 'FieldsController.update').as('fields.update')
// Route.delete('fields/:id', 'FieldsController.destroy').as('fields.destroy')
Route.resource('venues', 'VenuesController').apiOnly()
Route.resource('fields', 'FieldsController').apiOnly()
