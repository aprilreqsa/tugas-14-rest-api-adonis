import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BookingValidator from 'App/Validators/BookingValidator';
import VenueValidator from 'App/Validators/VenueValidator'
// import Database from '@ioc:Adonis/Lucid/Database';
import Venue from 'App/Models/Venue';

export default class VenuesController {
    public async store({request, response}: HttpContextContract){
        try{
            await request.validate(VenueValidator);
            // let newUserId = await Database.table('venues').returning('id').insert({
            //     name : request.input('name'),
            //     address : request.input('address'),
            //     phone : request.input('phone')
            // })
            let newVenue = await new Venue()
            newVenue.name = request.input('name')
            newVenue.address = request.input('address')
            newVenue.phone = request.input('phone')
            response.created({message: 'created!', newID: newVenue})
            
        }catch (error){
            response.badRequest(error.messages)
        }
    }

    public async booking({request, response}: HttpContextContract){
        try{
             await request.validate(BookingValidator);
        }catch (error){
            response.badRequest(error.messages)
        }
    }
    public async index({response, request}: HttpContextContract){
        // let venue = await Database.from('venues').select('*')
        if (request.qs().name){
            let name = request.qs().name
            let venuesFiltered = await Venue.findBy('name', name)
            return response.status(200).json({message: 'success', data: venuesFiltered})
        }
        let venues = await Venue.all()
        response.status(200).json({message: 'success', data: venues})
    }
    public async show({params, response}: HttpContextContract){
        // let venue = await Database.from('venues').where('id', params.id).select('id','name','address','phone').first()
        let venue = await Venue.find(params.id)
        return response.ok({message: 'success get venues with id', data: venue})
    }
    public async update({request, response, params}: HttpContextContract){
            let id = params.id
            // await Database.from('venues').where('id',id).update({
            //     name : request.input('name'),
            //     address : request.input('address'),
            //     phone : request.input('phone')
            // })
            let venue = await Venue.findOrFail(id)
            venue.name = request.input('name')
            venue.address =  request.input('address')
            venue.phone =  request.input('phone')
            venue.save()
            return response.ok({message : 'updated!'})
    }
    public async destroy({params, response}: HttpContextContract){
        // await Database.from('venues').where('id', params.id).delete()
        let venue = await Venue.findOrFail(params.id)
        venue.delete()
        return response.ok({message: 'deleted!'})
    }
    
}
