import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import Database from '@ioc:Adonis/Lucid/Database'
import FieldValidator from 'App/Validators/FieldValidator'
import Field from 'App/Models/Field'

export default class FieldsController {
        public async store({request, response}: HttpContextContract ){
            try{
                await request.validate(FieldValidator)
                // let newField = await Database.table('fields').returning('id').insert({
                //     name : request.input('name'),
                //     type : request.input('type'),
                //     venue_id : request.input('venue_id')
                // })
                let newFields = await new Field()
                newFields.name = request.input('name')
                newFields.type = request.input('type')
                newFields.venue_id = request.input('venue_id')
                response.created({message: 'created!', Field: newFields})
            }catch (error){
                response.badRequest(error.messages)
            }
            
        }
        public async index({response, request}: HttpContextContract){
            // let field = await Database.from('fields').select('*')
            if (request.qs().name){
                let name = request.qs().name
                let field = await Field.findBy('name', name)
                return response.ok({message: 'success', data: field})
            }
            let field = await Field.all()
            response.ok({message: 'success', data: field})
        }
        public async show({params, response}: HttpContextContract){
            // let field = await Database.from('fields').where('id', params.id).select('id','name','type','venue_id').first()
            let field = await Field.findOrFail(params.id)
            response.ok({message: 'succes', data: field})
        }
        public async update({params, request, response}: HttpContextContract){
            // await Database.from('fields').where('id', params.id).update({
            //     name : request.input('name'),
            //     type : request.input('type'),
            //     venue_id : request.input('venue_id')
            // })
            let id = params.id
            let field = await Field.findOrFail(id)
            field.name = request.input('name')
            field.type = request.input('type')
            field.venue_id = request.input('venue_id')
            field.save()
            return response.ok({message: 'updated!'})
        }
        public async destroy({params, response}: HttpContextContract){
            // await Database.from('fields').where('id', params.id).delete()
            let field = await Field.findOrFail(params.id)
            field.delete()
            return response.ok({message: 'deleted!'})
        }
}
